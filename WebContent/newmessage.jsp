<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>
<div class="main-contents">
<c:if test= "${ not empty errorMessages }">
	<ul class="errorMessages">
	<li>error</li>
		<c:forEach items="${errorMessages}" var="message">
				<li>　　※<c:out value="${message}"/>
		</c:forEach>
	</ul>
<c:remove var="errorMessages" scope="session"/>
</c:if>
<ul class="header">
	<li><p>MENU</p></li>
	<li><a href="./">ホーム</a></li>
	<li><a href="newMessage">新規投稿</a></li>
</ul>
<div class="content">
<div class="title">新規投稿作成</div>
<div class="form-area">
	<form action="newMessage" method="post">
	<ul class="newmessage">
		<li>件名(30文字以内)<br />
		<input type="text" name="subject" value="${message.subject}" >
		</li>
		<li>本文(1000文字以内)<br />
		<textarea name="text" cols="100" rows="5" class="tweet-box" >${message.text}</textarea>
		<br /></li>
		<li>カテゴリー(10文字以内)<br />
		<input type="text" name="category" value="${message.category}" >
		<br /></li>
		<li><br><input type="submit" value="投稿"></li>
		<c:remove var="message" scope="session"/>
	</ul>
	</form>


<br>
<br><a href="./">戻る</a>
</div>
</div>
<div class="copyright">Copyright(c)Ogiwara Takuya</div>
</div>
</body>
</html>