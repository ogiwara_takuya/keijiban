<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー登録</title>
</head>
<body>
<div class="main-contents">

<c:if test= "${ not empty errorMessages }">
	<ul class="errorMessages">
	<li>error</li>
		<c:forEach items="${errorMessages}" var="message">
				<li>　　※<c:out value="${message}"/>
		</c:forEach>
	</ul>
	<c:remove var="errorMessages" scope="session"/>
</c:if>
<ul class="header">
	<li><p>MENU</p></li>
	<li><a href="management">管理画面</a></li>
</ul>
<div class="content">
<div class="title">ユーザーの編集</div>
<form action="signup" method="post"><br />

	<ul class="setting-item">
	<li>ログインID</li>
	<li><input name="loginId" value="${inputUser.loginId}" id="loginId" /><br /></li>
	</ul>

	<ul class="setting-item">
	<li>名前</li>
	<li><input name="name" value="${inputUser.name}" id="name" /><br /></li>
	</ul>

	<ul class="setting-item">
	<li>パスワード</li>
	<li><input name="password" type="password" id="password" /><br /></li>
	</ul>

	<ul class="setting-item">
	<li>パスワード(確認用)</li>
	<li><input name="confirmationPass" type="password" id="confirmationPass" /><br /></li>
	</ul>

	<ul class="setting-item">
	<li>支店</li>
	<li><select name="branchId">
	<c:if test="${inputUser.branchId == 1 }">
		<option value="1" selected>本社</option>
	</c:if>
	<c:if test="${inputUser.branchId != 1 }">
		<option value="1">本社</option>
	</c:if>
	<c:if test="${inputUser.branchId == 2 }">
		<option value="2" selected>支店A</option>
	</c:if>
	<c:if test="${inputUser.branchId != 2 }">
		<option value="2">支店A</option>
	</c:if>
	<c:if test="${inputUser.branchId == 3 }">
		<option value="3" selected>支店B</option>
	</c:if>
	<c:if test="${inputUser.branchId != 3 }">
		<option value="3">支店B</option>
	</c:if>
	</select></li>
	</ul>

	<ul class="setting-item">
	<li>部署・役職</li>
	<li><select name="positionId">
	<c:if test="${inputUser.positionId == 1 }">
		<option value="1" selected>総務人事担当</option>
	</c:if>
	<c:if test="${inputUser.positionId != 1 }">
		<option value="1">総務人事担当</option>
	</c:if>
	<c:if test="${inputUser.positionId == 2 }">
		<option value="2" selected>情報管理担当</option>
	</c:if>
	<c:if test="${inputUser.positionId != 2 }">
		<option value="2">情報管理担当</option>
	</c:if>
	<c:if test="${inputUser.positionId == 3 }">
		<option value="3" selected>店長</option>
	</c:if>
	<c:if test="${inputUser.positionId != 3 }">
		<option value="3">店長</option>
	</c:if>
	<c:if test="${inputUser.positionId == 4 }">
		<option value="4" selected>社員</option>
	</c:if>
	<c:if test="${inputUser.positionId != 4 }">
		<option value="4">社員</option>
	</c:if>
	</select></li>
	</ul>
	<c:remove var="inputUser" scope="session"/>

	<br><input type="submit" value="登録" /> <br />

	<br><a href="management">戻る</a>
</form>
</div>
</div>
<div class="copyright">Copyright(c)Ogiwara Takuya</div>
</body>
</html>