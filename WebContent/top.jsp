<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
</head>
<body>
<div class="main-contents">
<c:if test= "${ not empty errorMessages }">
	<ul class="errorMessages">
	<li>error</li>
		<c:forEach items="${errorMessages}" var="message">
				<li>　　※<c:out value="${message}"/>
		</c:forEach>
	</ul>
	<c:remove var="errorMessages" scope="session"/>
</c:if>
<ul class="header">
	<li><p>MENU</p></li>
	<li><a href="./">ホーム</a></li>
	<li><a href="newMessage">新規投稿</a></li>
	<c:if test="${loginUser.positionId == 1}">
		<li><a href="management">管理画面</a></li>
	</c:if>
	<br>
	<li><a href="logout">ログアウト</a></li>
</ul>
<br />
<div class="top-content">

<div class="search">
	<form action="./" method="get">
		<div class="search-title">投稿を検索</div>

		<ul class="search-category">
		<li>カテゴリー</li>
		<li><input name="category" id="category" value="${search.get(0)}" /></li>
		</ul>

		<ul class="search-date">
		<li>日時</li>
		<li><a>
		<c:if test="${ not empty search.get(1)}">
			<input type="date" name="dateBefore" value="${search.get(1)}"> ～
		</c:if>
		<c:if test="${ not empty search.get(2)}">
			<input type="date" name="dateAfter" value="${search.get(2)}">
		</c:if>

		<c:if test="${ empty search.get(1)}">
			<input type="date" name="dateBefore"> ～
		</c:if>
		<c:if test="${ empty search.get(2)}">
			<input type="date" name="dateAfter">
		</c:if>
		<input id="search-submit" type="submit" name="search"  value="検索" />
		<c:remove var="search" scope="session"/>
		</a></li>
		</ul>


	</form>
</div>
<br>
	<c:forEach items="${messages}" var="message">
		<div class="message">
			<div class="is-messege">
				<input type="hidden" name="messageId" value="${message.id}"/>

				<ul class="message-user">
					<li><p><c:out value="${message.name}" />  さんの投稿</p></li>
					<li><div class="date">　<fmt:formatDate value="${message.updatedAt}" pattern="yyyy/MM/dd HH:mm:ss" /></div></li>
				</ul>

				<ul class="message-item">
				<li><p><font size="-1" style="opacity: 0.7;">件名</font><br />
				<a><c:out value="${message.subject}" /></a></p></li>
				</ul>
				<ul class="message-text">
				<li><font size="-1" style="opacity: 0.7;">本文</font><br />
				<a><c:forEach var="text" items="${fn:split(message.text,'
				')}">
					<c:out value="　${text}"></c:out><br />
				</c:forEach></a>
				</li>
				</ul>

				<div class="message-category">
				<p><font size="-1" style="opacity: 0.7;">カテゴリー</font>
				<c:out value="${message.category}" /></p></div>
			<div class="message-button">
				<form action="deleteMessage" method="post">
				<c:if test="${message.loginId == loginUser.loginId }">
					<input type="hidden" name="messageId" value="${message.id}"/>
					<input id="login-submit" type="submit" name="messageDelete" value="投稿を削除する" onClick="return confirm('本当に削除しますか？')">
				</c:if>
				</form>
			</div>

			<div class="comment">

				<form action="comment" method="post">
					<div class="comment-submit">コメントする(500文字以内)<br />
					<a><textarea name="comment" cols="100" rows="5" class="text-box" >${comment}</textarea></a>
					<br />
					<input type="hidden" name="messageId" value="${message.id}"/>
					<div class="comment-submit-button"><input id="submit" type="submit" value="コメントの送信"></div>
					<c:remove var="comment" scope="session"/></div>
				</form>
			</div>
			</div>

					<c:forEach items="${comments}" var="comment">
						<c:if test="${message.id == comment.messageId}" >
						<div class="is-comment">
						<ul class="comment-name">
							<li><input type="hidden" name="commentId" value="${comment.id}"/></li>
							<li><p><c:out value="${comment.name}" />  さんからのコメント</p></li>
						</ul>

						<div class="comment-text"><font size="-1" style="opacity: 0.7;">コメント<br /></font>
						<c:forEach var="commentText" items="${fn:split(comment.text,'
						')}">
							<a><c:out value="${commentText}" /></a>
						</c:forEach>
						</div>

						<div class="date">　　　　　<fmt:formatDate value="${comment.updatedAt}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

						<form action="deleteMessage" method="post">
						<div class="comment-button">
							<c:if test="${comment.loginId == loginUser.loginId }">
								<input type="hidden" name="commentId" value="${comment.id}"/>
								<input id="login-submit" type="submit" name="commentDelete" value="コメントを削除する" onClick="return confirm('本当に削除しますか？')">
							</c:if>
							</div>
						</form>
						</div>
					</c:if>
				</c:forEach>
			</div>
	</c:forEach>
</div>
<div class="copyright">Copyright(c)Ogiwara Takuya</div>
</div>
</body>
</html>