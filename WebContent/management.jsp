<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
</head>
<body>
<div class="main-contents">
<ul class="header">
	<li><p>MENU</p></li>
	<li><a href="./">ホーム</a></li>
	<li><a href="signup">新規ユーザー作成</a></li>
		<br>
	<li><a href="logout">ログアウト</a></li>
</ul>
<c:if test= "${ not empty errorMessages }">
	<ul class="errorMessages">
	<li>error</li>
		<c:forEach items="${errorMessages}" var="message">
				<li>　　※<c:out value="${message}"/>
		</c:forEach>
	</ul>
	<c:remove var="errorMessages" scope="session"/>
</c:if>
<div class="management-content">
<div class="title">ユーザーの管理</div>
	<table class="nav">
		<tr>
			<th>ログインID</th>
			<th>名前</th>
			<th>支店</th>
			<th>部署・役職</th>
			<th>アカウント停止</th>
			<th>編集</th>
		</tr>
		<tr>
		<c:forEach items="${usersInfo}" var="user">
		<tr>
			<td><c:out value="${user.loginId}"/></td>
			<td><c:out value="${user.name}" /></td>
			<td><c:out value="${user.branchName}" /></td>
			<td><c:out value="${user.positionName}" /></td>

			<td>
			<form action="isDeleted" method="post">
				<c:if test="${user.id != loginUser.id }">
				<c:if test="${user.isDeleted == 0}">
					<input type="submit" value="アカウントを停止する" onClick="return confirm('本当に停止しますか？')">
				</c:if>

					<c:if test="${user.isDeleted == 1}">
						<font size="2" color="red">※アカウント停止中</font><br />
						<input type="submit" value="アカウントを復旧する" onClick="return confirm('復旧してよろしいですか？')">
					</c:if>

					<input type="hidden" name="id" value="${user.id}" />
					<input type="hidden" name="isDeleted" value="${user.isDeleted}" />
					<input type="hidden" name="updated_at" value="${user.updatedAt}" />
				</c:if>

				<c:if test="${user.id == loginUser.id }">
					<font size="1">自分のアカウント停止は<br>　　　　　　　できません</font>
				</c:if>
			</form>
			</td>

			<td><p><a href="setting?userId=${user.id}">ユーザーを編集</a><p></td>
		</tr>
		</c:forEach>
	</table>


<%--
<c:forEach items="${usersInfo}" var="user">
	<div class="management">
		<div class = "user-info">
			<br><span Class="info-login-id"><c:out value="${user.loginId}"/> </span>
			<span Class="info-name">	<c:out value="${user.name}" /> </span>
			<span Class="info-branch-name"><c:out value="${user.branchName}" /> </span>
			<span Class="info-position-name"><c:out value="${user.positionName}" /> </span>
		</div>
		<div class="is-deleted-button">
			<form action="isDeleted" method="post">
			<c:if test="${user.id != loginUser.id }">
				<c:if test="${user.isDeleted == 0}">
					<input type="submit" value="アカウント停止にする" onClick="return confirm('本当に停止しますか？')">
				</c:if>

				<c:if test="${user.isDeleted == 1}">
					<label for="isDeleted">アカウント停止中</label>
					<input type="submit" value="アカウントを復旧する" onClick="return confirm('復旧してよろしいですか？')">
				</c:if>

				<input type="hidden" name="id" value="${user.id}" />
				<input type="hidden" name="isDeleted" value="${user.isDeleted}" />
				<input type="hidden" name="updated_at" value="${user.updatedAt}" />
			</c:if>
				<c:if test="${user.id == loginUser.id }">
					<br> 自分のアカウント停止はできません <br />
				</c:if>
			</form>
		</div>

			<a href="setting?userId=${user.id}">ユーザーを編集</a>
			<br>------------------------------------------------------------------<br />


	</div>
</c:forEach>--%>
<br><a href="./">戻る</a>
</div>
<div class="copyright">Copyright(c)takuya ogiwara</div>
</div>
</body>
</html>