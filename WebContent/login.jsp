<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン</title>
</head>
<body>
<div class="main-contents">

<c:if test= "${ not empty errorMessages }">
	<ul class="errorMessages">
	<li>error</li>
		<c:forEach items="${errorMessages}" var="message">
				<li>　　※<c:out value="${message}"/>
		</c:forEach>
	</ul>
	<c:remove var="errorMessages" scope="session"/>
</c:if>
<div class="login-content">
<div class="login-title">
	掲示板<br />
</div>
<form action="login" method="post"><br />
<ul class="login-item">
	<li><label for="loginId">ログインID</label></li>
	<li><input name="loginId" value="${loginId}" id="loginId"/><br /></li>
</ul>
<ul class="login-item">
	<li><label for="password">パスワード</label>
	<li><input name="password" type="password" id="password"/><br /></li>
</ul>
<div class="login-submit">
	<input id="submit" type="submit" value="ログイン" /> <br />
</div>
</form>
</div>

<div class="copyright">Copyright(c)takuya ogiwara</div>
</div>
</body>
</html>