package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao{
	public void insert(Connection connection, Comment comment){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("user_id");
			sql.append(", message_id");
			sql.append(", text");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(")VALUES (");
			sql.append("?");//user_id
			sql.append(",?");//message_id
			sql.append(",?");//text
			sql.append(", CURRENT_TIMESTAMP");//created_at
			sql.append(", CURRENT_TIMESTAMP");//updated_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());


			ps.setInt(1, comment.getUserId());
			ps.setInt(2, comment.getMessageId());
			ps.setString(3, comment.getText());

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
	public Comment deleteComment(Connection connection, int id){

		PreparedStatement ps = null;
		try{
			String sql = "Delete FROM comments WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ps.executeUpdate();
			return null;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
}

