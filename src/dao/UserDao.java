package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao{
	public void insert(Connection connection, User user){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", is_deleted");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(")VALUES (");
			sql.append("?");//login_id
			sql.append(",?");//password
			sql.append(",?");//name
			sql.append(",?");//branch_id
			sql.append(",?");//position_id
			sql.append(",0");//is_deleted
			sql.append(", CURRENT_TIMESTAMP");//created_at
			sql.append(", CURRENT_TIMESTAMP");//updated_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());


			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getPositionId());


			/*if(user.getIcon() == null){
				ps.setObject(6, null);
			}else{
				ps.setBinaryStream(6, new ByteArrayInputStream(user.getIcon()));
			}*/

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
	public User getUser(Connection connection, String loginId, String password){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_deleted = 0" ;

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true){
				return null;
			}else if (2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size");
			}else{
				return userList.get(0);
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
	private List<User> toUserList(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<User>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isDeleted = rs.getInt("is_deleted");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setIsDeleted(isDeleted);
				user.setCreatedAt(createdAt);
				user.setUpdatedAt(updatedAt);

				ret.add(user);
			}
			return ret;
		}finally{
			rs.close();
		}
	}
	public User getUser(Connection connection, int id){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			UserDao userDao = new UserDao();
			List<User> userList = userDao.toUserList(rs);
			if(userList.isEmpty() == true){
				return null;
			}else if (2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size");
			}else{
				return userList.get(0);
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
	public void update(Connection connection, User user){
		//?を使えるようにしてる
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", name = ?");

			if(user.getPassword() != null){
				sql.append(", password = ?");
			}
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");
			sql.append(" AND");
			sql.append(" updated_at = ?");

			ps = connection.prepareStatement(sql.toString());


			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());

			if(user.getPassword() != null){
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getPositionId());
				ps.setInt(6, user.getId());
				ps.setTimestamp(7, new Timestamp(user.getUpdatedAt().getTime()));

			}else{
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getPositionId());
				ps.setInt(5, user.getId());
				ps.setTimestamp(6, new Timestamp(user.getUpdatedAt().getTime()));
			}

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
	public User checkUser(Connection connection, String loginId){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			UserDao userDao = new UserDao();
			List<User> userList = userDao.toUserList(rs);
			if(userList.isEmpty() == true){
				return null;
			}else if (2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size");
			}else{
				return userList.get(0);
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
}

