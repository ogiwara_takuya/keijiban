package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao{
	public List<UserComment> getUserComments(Connection connection, int num){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append( "SELECT * FROM user_comment ");
			sql.append( "ORDER BY updated_at DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);

			return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs) throws SQLException {
		List<UserComment> ret = new ArrayList<UserComment>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int messageId = rs.getInt("message_id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");;
				String text = rs.getString("text");
				Timestamp updetedAt = rs.getTimestamp("updated_at");

				UserComment message = new UserComment();
				message.setId(id);
				message.setUserId(userId);
				message.setMessageId(messageId);
				message.setLoginId(loginId);
				message.setName(name);
				message.setText(text);
				message.setUpdatedAt(updetedAt);

				ret.add(message);
			}
			return ret;
		}finally{
			rs.close();
		}
	}
}
