package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
//import java.sql.Statement;
import java.sql.Timestamp;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class IsDeletedDao{
	public void update(Connection connection, User user){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");
			sql.append(" AND");
			sql.append(" updated_at = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIsDeleted());
			ps.setInt(2, user.getId());
			ps.setTimestamp(3, new Timestamp(user.getUpdatedAt().getTime()));

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
}
