package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao{
	public List<UserMessage> getUserMessages(Connection connection, int num){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append( "SELECT * FROM user_message ");
			sql.append( "ORDER BY updated_at DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);

			return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs) throws SQLException {
		List<UserMessage> ret = new ArrayList<UserMessage>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp updetedAt = rs.getTimestamp("updated_at");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setUserId(userId);
				message.setLoginId(loginId);
				message.setName(name);
				message.setSubject(subject);
				message.setText(text);
				message.setCategory(category);
				message.setUpdatedAt(updetedAt);

				ret.add(message);
			}
			return ret;
		}finally{
			rs.close();
		}
	}
}
