package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Management;
import exception.SQLRuntimeException;

public class ManagementDao{
	public List<Management> getManagement(Connection connection, int num){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append( "SELECT * FROM user_info ");
			//sql.append( "ORDER BY id DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Management> ret = toManagementList(rs);

			return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}

	private List<Management> toManagementList(ResultSet rs) throws SQLException {
		List<Management> ret = new ArrayList<Management>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String branchName = rs.getString("branch_name");
				String positionName = rs.getString("position_name");
				int isDeleted = rs.getInt("is_deleted");
				Timestamp updetedAt = rs.getTimestamp("updated_at");

				Management usersInfo = new Management();
				usersInfo.setId(id);
				usersInfo.setLoginId(loginId);
				usersInfo.setName(name);
				usersInfo.setBranchName(branchName);
				usersInfo.setPositionName(positionName);
				usersInfo.setIsDeleted(isDeleted);
				usersInfo.setUpdatedAt(updetedAt);

				ret.add(usersInfo);
			}

			return ret;
		}finally{
			rs.close();
		}
	}
}

