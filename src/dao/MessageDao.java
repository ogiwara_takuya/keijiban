package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao{
	public void insert(Connection connection, Message message){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("user_id");
			sql.append(", subject");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(")VALUES (");
			sql.append("?");//user_id
			sql.append(",?");//subject
			sql.append(",?");//text
			sql.append(",?");//category
			sql.append(", CURRENT_TIMESTAMP");//insert_date
			sql.append(", CURRENT_TIMESTAMP");//update_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
//			ps.setInt(1, message.getId());
			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getSubject());
			ps.setString(3, message.getText());
			ps.setString(4, message.getCategory());

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
	public Message getMessage(Connection connection, int id){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			MessageDao messageDao = new MessageDao();
			List<Message> messageList = messageDao.toMessageList(rs);
			if(messageList.isEmpty() == true){
				return null;
			}else if (2 <= messageList.size()){
				throw new IllegalStateException("2 <= messageList.size");
			}else{
				return messageList.get(0);
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
	private List<Message> toMessageList(ResultSet rs) throws SQLException {
		List<Message> ret = new ArrayList<Message>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");

				Message message = new Message();
				message.setId(id);
				message.setUserId(userId);
				message.setSubject(subject);
				message.setText(text);
				message.setCategory(category);
				message.setCreatedAt(createdAt);
				message.setUpdatedAt(updatedAt);

				ret.add(message);
			}
			return ret;
		}finally{
			rs.close();
		}
	}
	public Message deleteMessage(Connection connection, int id){

		PreparedStatement ps = null;
		try{
			String sql = "Delete FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ps.executeUpdate();
			return null;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			try {
				if(ps != null){
					ps.close();
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		}
	}
}
