package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Management;
import service.ManagementService;


@WebServlet(urlPatterns = "/management")
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	 @Override
     protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");

		 List<Management> usersInfo = new ManagementService().getUsersInfo();

		 request.setAttribute("usersInfo", usersInfo);

		 request.getRequestDispatcher("/management.jsp").forward(request, response);

		 request.setCharacterEncoding("UTF-8");
			//アカウント停止文の処理
		}

}
