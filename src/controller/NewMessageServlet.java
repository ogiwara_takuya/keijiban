package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;;


@WebServlet(urlPatterns = "/newMessage")
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("/newmessage.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if(isValid(request, messages) == true){
			User user = (User)session.getAttribute("loginUser");

			Message message = new Message();
			message.setUserId(user.getId());
			message.setSubject(request.getParameter("subject"));
			message.setText(request.getParameter("text"));
			message.setCategory(request.getParameter("category"));

			new MessageService().register(message);

			request.setAttribute("userMessage", messages);
			response.sendRedirect("./");
		}else{
			request.setAttribute("errorMessages", messages);
//			response.sendRedirect("newMessage");
			request.getRequestDispatcher("/newmessage.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String>messages){


		Message message = new Message();

		message.setSubject(request.getParameter("subject"));
		message.setText(request.getParameter("text"));
		message.setCategory(request.getParameter("category"));

		if(StringUtils.isBlank(message.getSubject()) == true){
			messages.add("件名を入力してください");
		}
		if(30 < message.getSubject().length()){
			messages.add("件名は30文字以内で入力してください");
		}
		if(StringUtils.isBlank(message.getText()) == true){
			messages.add("本文を入力してください");
		}
		if(1000 < message.getText().length()){
			messages.add("本文は1000文字以内で入力してください");
		}
		if(StringUtils.isBlank(message.getCategory()) == true){
			messages.add("カテゴリーを入力してください");
		}
		if(10 < message.getCategory().length()){
			messages.add("カテゴリーは10文字以内で入力してください");
		}

		request.setAttribute("message", message);

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}

