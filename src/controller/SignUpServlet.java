package controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;


@WebServlet(urlPatterns = "/signup")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("/signup.jsp").forward(request, response);

	}

	 @Override
     protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");

		 List<String>messages = new ArrayList<String>();

		 if(isValid(request, messages) == true){
			User user = new User();
        	user.setLoginId(request.getParameter("loginId"));
        	user.setPassword(request.getParameter("password"));
        	user.setName(request.getParameter("name"));

        	String branchId = request.getParameter(String.valueOf("branchId"));
        	user.setBranchId(Integer.parseInt(branchId));

        	String positionId = request.getParameter(String.valueOf("positionId"));
        	user.setPositionId(Integer.parseInt(positionId));

        	new UserService().register(user);

        	messages.add("新規ユーザーを登録しました");
        	request.setAttribute("errorMessages", messages);

        	response.sendRedirect("management");

		 }else{
        	request.setAttribute("errorMessages", messages);
//	        	response.sendRedirect("signup");
        	request.getRequestDispatcher("/signup.jsp").forward(request, response);
		 }
	 }
	 private boolean isValid (HttpServletRequest request, List<String>messages){

		 String loginId = request.getParameter("loginId");
		 String password = request.getParameter("password");
		 String confirmationPass = request.getParameter("confirmationPass");
		 String name = request.getParameter("name");
		 String branchId = request.getParameter("branchId");
		 String positionId = request.getParameter("positionId");
		 User checkUser = new UserService().checkUser(loginId);


		 if(StringUtils.isBlank(loginId) == true){
			 messages.add("ログインIDを入力してください");
		 }else{
			 if(!loginId.matches("[0-9a-zA-Z]+")){
			     messages.add("ログインIDは半角英数で入力してください");
			}
			 if(loginId.length() < 6 || 20 < loginId.length() ){
			     messages.add("ログインIDの文字数は6文字以上、20文字以下で入力してください");
			}
		 }
     	 if(checkUser != null){
 			messages.add("そのログインIDは既に使用されています");
 		 }
		 if(StringUtils.isBlank(password) == true){
			 messages.add("パスワードを入力してください");
		 }else{
			 if(password.length() < 6 || 20 < password.length() ){
			     messages.add("パスワードの文字数は6文字以上、20文字以下で入力してください");
			 	}
		 }

		 if(StringUtils.isBlank(confirmationPass) == true){
			 messages.add("確認用パスワードを入力してください");
		 }
		 if(!password.equals(confirmationPass)){
			 messages.add("パスワードと確認用パスワードが一致しません");
		 }
		 if(StringUtils.isBlank(name) == true){
			 messages.add("名前を入力してください");
		 }
		 if(StringUtils.isBlank(branchId) == true){
			 messages.add("支店を入力してください");
		 }
		 if(StringUtils.isBlank(positionId) == true){
			 messages.add("役職を入力してください");
		 }



     	 User user = new User();
     	 user.setLoginId(request.getParameter("loginId"));
    	 user.setName(request.getParameter("name"));
    	 user.setBranchId(Integer.parseInt(branchId));
    	 user.setPositionId(Integer.parseInt(positionId));

    	 request.setAttribute("inputUser",user);

		 if(messages.size() == 0){
			 return true;
		 }else{
			 return false;
		 }
	 }
}

