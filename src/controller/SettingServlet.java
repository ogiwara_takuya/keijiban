package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

//URL取得
@WebServlet(urlPatterns = {"/setting"})
@MultipartConfig(maxFileSize = 100000)
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(urlValid(request, messages) == true){

			User editUser =  new UserService().getUser(Integer.parseInt(request.getParameter(String.valueOf("userId"))));

			if(editUser == null){
				messages.add("編集するユーザーが存在しません");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("management");
//				request.getRequestDispatcher("management").forward(request, response);
			}else{

				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
			}

		}else{

			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
//			request.getRequestDispatcher("management").forward(request, response);
		}
	}

	private boolean urlValid(HttpServletRequest request, List<String>messages){
		String strUserId = request.getParameter("userId");

		if(StringUtils.isBlank(strUserId) == true){
			messages.add("編集するユーザーが存在しません");
		}else{
			try{
				Integer.parseInt(strUserId);

			}catch(NumberFormatException e){
				messages.add("編集するユーザーが存在しません");
			}
		}

		if(messages.size() == 0){
			 return true;
		}else{
			 return false;
		}
	}

	 @Override
     protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException, ServletException {
		 request.setCharacterEncoding("UTF-8");

		 HttpSession session = request.getSession();
		 //リスト作成(エラーメッセージ用)
		 List<String> messages = new ArrayList<String>();

		 //editUser情報を取得
		 User editUser = getEditUser(request);

		 request.setAttribute("editUser", editUser);

		 //値の真偽
		 if(isValid(request, messages) == true){
			 try{

				 new UserService().update(editUser);

			 }catch(NoRowsUpdatedRuntimeException e){
				 request.removeAttribute("editUser");
				 messages.add("他の人によって更新されています。更新のデータを表示しました。データを確認してください。");
				 return;
			 }

			 if(editUser.getId() == Integer.parseInt(request.getParameter("loginUserId"))){
				 editUser = new UserService().getUser(editUser.getId());
				 session.setAttribute("loginUser", editUser);
			 }

			 request.removeAttribute("editUser");
			 response.sendRedirect("management");
		 }else{
			 request.setAttribute("errorMessages", messages);
//			 request.getRequestDispatcher("setting?userId=" + request.getParameter("id")).forward(request, response);
//			 response.sendRedirect("setting?userId=" + request.getParameter("id"));
			 request.getRequestDispatcher("setting.jsp").forward(request, response);
		 }
	 }

	 private User getEditUser(HttpServletRequest request) throws IOException, ServletException {

		 //HttpSession session = request.getSession();
		 //User editUser = (User) request.getAttribute("editUser");
		 User editUser = new User();

		 String id = request.getParameter(String.valueOf("id"));
		 editUser.setId(Integer.parseInt(id));

		 editUser.setLoginId(request.getParameter("loginId"));
		 editUser.setName(request.getParameter("name"));
		 if(StringUtils.isBlank(request.getParameter("password")) != true){
			 editUser.setPassword(request.getParameter("password"));
		 }
		 String branchId = request.getParameter(String.valueOf("branchId"));
		 editUser.setBranchId(Integer.parseInt(branchId));
		 String positionId = request.getParameter(String.valueOf("positionId"));
		 editUser.setPositionId(Integer.parseInt(positionId));

		 SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 String updatedAt = request.getParameter(String.valueOf("updatedAt"));
		 try {
			editUser.setUpdatedAt(date.parse(updatedAt));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		 return editUser;
		 }

	 private boolean isValid(HttpServletRequest request, List<String>messages){
		 String strId = request.getParameter(String.valueOf("id"));
		 int id = Integer.parseInt(strId);
		 String loginId = request.getParameter("loginId");
		 String name = request.getParameter("name");
		 String branchId = request.getParameter(String.valueOf("branchId"));
		 String positionId = request.getParameter(String.valueOf("positionId"));
		 String password = request.getParameter("password");
		 String confirmationPass = request.getParameter("confirmationPass");
		 User user = new UserService().checkUser(loginId);

		 if(StringUtils.isBlank(loginId) == true){
			 messages.add("ログインIDを入力してください");
		 }else{
			 if(!loginId.matches("[0-9a-zA-Z]+")){
			     messages.add("ログインIDは半角英数で入力してください");
			}
			 if(loginId.length() < 6 || 20 < loginId.length() ){
			     messages.add("ログインIDの文字数は6文字以上、20文字以下で入力してください");
			}
		 }
		 if(user != null && user.getId() != id){
			 messages.add("そのログインIDは既に使用されています");
		 }

		 if(!password.equals(confirmationPass)){
			 messages.add("パスワードと確認用パスワードが一致しません");
		 }
		 if(StringUtils.isBlank(name) == true){
			 messages.add("名前を入力してください");
		 }else{
			 if(10 < name.length()){
			     messages.add("名前の文字数は10文字以下で入力してください");
			}
		 }
		 if(branchId.equals("0")){
			 messages.add("支店を入力してください");
		 }
		 if(positionId.equals("0")){
			 messages.add("役職・部署を入力してください");
		 }

		 if(messages.size() == 0){
			 return true;
		 }else{
			 return false;
		 }
	 }
}
