package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.SearchService;


@WebServlet(urlPatterns = "/index.jsp")
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//		ユーザーの入力値の取得
		String category = request.getParameter("category");

		String dateBefore = request.getParameter("dateBefore");
		String dateAfter = request.getParameter("dateAfter");

		List<String> searchList = new ArrayList<String>();

		searchList.add(category);
		searchList.add(dateBefore);
		searchList.add(dateAfter);

//		今日の時間取得
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date todayDate = new Date();
		String todayDateFormat = dateFormat.format(todayDate);


		if(StringUtils.isBlank(dateBefore) == true){
			dateBefore = "2018-01-01";
		}
		if(StringUtils.isBlank(dateAfter) == true){
			dateAfter = todayDateFormat;
		}

		String date = dateBefore+ "," + dateAfter;

		List<UserMessage> messages = new SearchService().getMessage(date, category);

		List<UserComment> comments = new CommentService().getComment();



		request.setAttribute("search", searchList);

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);


		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

}
