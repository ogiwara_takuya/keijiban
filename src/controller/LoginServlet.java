package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;

//URLを呼び出す
@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//jspを呼び出す
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException, ServletException {


		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}

	 @Override
     protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException, ServletException {

		 //リクエストから情報を取得
		 String loginId = request.getParameter("loginId");
		 String password = request.getParameter("password");

		 //serviceからログイン情報取得
		 LoginService loginService = new LoginService();
		 User user = loginService.login(loginId, password);

		 //sessionにコピー
		 HttpSession session = request.getSession();

		 if(user != null){
			 //Attributeで属性名と値をセット
			 session.setAttribute("loginUser", user);
			 //レスポンスに返す
			 response.sendRedirect("./");
		 }else{

			 List<String> messages = new ArrayList<String>();
			 messages.add("ログインに失敗しました。");
			 request.setAttribute("errorMessages", messages);
			 request.setAttribute("loginId", loginId);
			 request.getRequestDispatcher("/login.jsp").forward(request, response);

			 //リクエストの構文
        	 //request.setAttribute("errorMessages", messages);
        	 //RequestDispatcher rd = request.getRequestDispatcher("login");
        	 //rd.forward(request,response);
		 }
	 }
}
