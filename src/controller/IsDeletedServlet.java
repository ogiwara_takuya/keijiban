package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.IsDeletedService;
import service.UserService;


@WebServlet(urlPatterns = "/isDeleted")
public class IsDeletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	 @Override
     protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		User editUser =  new UserService().getUser(Integer.parseInt(request.getParameter(String.valueOf("id"))));
		System.out.println(editUser.getId());

		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String updatedAt = request.getParameter(String.valueOf("updated_at"));

		try {
			editUser.setUpdatedAt(date.parse(updatedAt));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		editUser.setIsDeleted(Integer.parseInt(request.getParameter(String.valueOf("isDeleted"))));

		if(editUser.getIsDeleted() == 0){
			editUser.setIsDeleted(1);
		}else{
			editUser.setIsDeleted(0);
		}
//		if(editUser.getIsDeleted() == 1){
//			editUser.setIsDeleted(0);
//		}
		new IsDeletedService().update(editUser);

		response.sendRedirect("management");

	}

}
