package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Message;
import beans.User;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = "/comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Message message = new MessageService()
				.getMessage(Integer.parseInt(request.getParameter(String.valueOf("messageId"))));
		request.setAttribute("message", message);

		request.getRequestDispatcher("/comment.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		List<String> message = new ArrayList<String>();

		if (isValid(request, message) == true) {
			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();

			comment.setUserId(user.getId());
			comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
			comment.setText(request.getParameter("comment"));

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", message);
			// request.getRequestDispatcher("top.jsp").forward(request,
			// response);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> message) {

		String comment = request.getParameter("comment");

		if (StringUtils.isBlank(comment) == true) {
			message.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			message.add("500文字以下で入力してください");
		}
		if (message.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
