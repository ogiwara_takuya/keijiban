package service;

import static utils.DBUtil.*;

//import java.io.ByteArrayOutputStream;
//import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import beans.User;
import dao.UserDao;
import exception.SQLRuntimeException;
import utils.CipherUtil;
//import chapter6.utils.StreamUtil;

public class LoginService {
	public User login(String loginId, String password) {

		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, loginId, encPassword);

			commit(connection);
			return user;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
}
