package service;

import static utils.DBUtil.*;

//import java.io.ByteArrayOutputStream;
//import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import beans.User;
import dao.IsDeletedDao;
import exception.SQLRuntimeException;
import utils.CipherUtil;

public class IsDeletedService {

	public void update(User user){

		Connection connection = null;
		try{
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			IsDeletedDao isDeletedDao =  new IsDeletedDao();
			isDeletedDao.update(connection, user);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
}