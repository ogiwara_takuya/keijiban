package service;

import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.UserCommentDao;
import exception.SQLRuntimeException;

public class CommentService {
	public void register(Comment comment) {

		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
	private static final int LIMIT_NUM = 1000;

	public List<UserComment> getComment(){
		Connection connection = null;
		try{
			connection = getConnection();

			UserCommentDao commentDao = new UserCommentDao();
			List<UserComment> ret = commentDao.getUserComments(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
	public Comment deleteComment(int id){

		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			Comment comment = commentDao.deleteComment(connection, id);

			commit(connection);

			return comment;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
}
