package service;

import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import beans.Management;
import dao.ManagementDao;
import exception.SQLRuntimeException;

public class ManagementService {
	private static final int LIMIT_NUM = 1000;
	public List<Management> getUsersInfo(){
		Connection connection = null;
		try{
			connection = getConnection();
			
			ManagementDao managementDao = new ManagementDao();
			List<Management> ret = managementDao.getManagement(connection, LIMIT_NUM);
			
			commit(connection);
			
			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
			
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
}