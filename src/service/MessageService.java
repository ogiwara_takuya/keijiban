package service;

import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;
import exception.SQLRuntimeException;

public class MessageService {
	public void register(Message message) {

		Connection connection = null;
		try{
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
	private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage(){
		Connection connection = null;
		try{
			connection = getConnection();

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}

	public Message getMessage(int id){

		Connection connection = null;
		try{
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			Message message = messageDao.getMessage(connection, id);

			commit(connection);

			return message;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
	public Message deleteMessage(int id){

		Connection connection = null;
		try{
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			Message message = messageDao.deleteMessage(connection, id);

			commit(connection);

			return message;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
}