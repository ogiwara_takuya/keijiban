package service;

import static utils.DBUtil.*;

//import java.io.ByteArrayOutputStream;
//import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import beans.User;
import dao.UserDao;
import exception.SQLRuntimeException;
import utils.CipherUtil;
//import chapter6.utils.StreamUtil;

public class UserService {
	public void register(User user) {
		Connection connection = null;
		try{
			connection = getConnection();

			//パスワード暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			//setDefaultIcon(user);

			//daoに移してる
			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
	public User getUser(int id){

		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, id);

			commit(connection);

			return user;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
public void update(User user){

		Connection connection = null;
		try{
			connection = getConnection();
			if(user.getPassword() != null){
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}

			UserDao userDao =  new UserDao();
			userDao.update(connection, user);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
public User checkUser(String loginId){

	Connection connection = null;
	try{
		connection = getConnection();

		UserDao userDao = new UserDao();
		User user = userDao.checkUser(connection, loginId);

		commit(connection);

		return user;
	}catch(RuntimeException e){
		rollback(connection);
		throw e;
	}catch(Error e){
		rollback(connection);
		throw e;

	}finally{
		try {
			if(connection != null){
				connection.close();
			}
		}catch (SQLException e) {
			rollback(connection);
			throw new SQLRuntimeException(e);
		}catch(Error e){
			rollback(connection);
			throw e;
		}
	}
}
}
