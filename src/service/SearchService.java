package service;

import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import beans.UserMessage;
import dao.SearchDao;
import exception.SQLRuntimeException;

public class SearchService {
	private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage(String date, String category){
		Connection connection = null;
		try{
			connection = getConnection();

			SearchDao searchDao = new SearchDao();
			List<UserMessage> ret = searchDao.getUserMessages(connection, LIMIT_NUM, date, category);

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			}catch (SQLException e) {
				rollback(connection);
				throw new SQLRuntimeException(e);
			}catch(Error e){
				rollback(connection);
				throw e;
			}
		}
	}
}