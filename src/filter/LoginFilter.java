package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

    	HttpSession session = ((HttpServletRequest)request).getSession();
//    	String url = ((HttpServletRequest) request).getRequestURI();
//    	System.out.println(url);

        //ログインフィルター
        User user = (User) session.getAttribute("loginUser");
        List<String> messages = new ArrayList<String>();

        if(!((HttpServletRequest)request).getServletPath().equals("/login")){
        	if(user == null){
        		if(!((HttpServletRequest)request).getServletPath().equals("/css/style.css")){

					 messages.add("ログインしていないので、アクセスできません。");
					 session.setAttribute("errorMessages", messages);
	//				 ((HttpServletResponse)response).sendRedirect("login");
					 request.getRequestDispatcher("/login.jsp").forward(request, response);

		        	return;
        		}
	        }
        }

        if(((HttpServletRequest)request).getServletPath().equals("/setting")){
		    if(user.getPositionId() != 1){
		    	if(!((HttpServletRequest)request).getServletPath().equals("/css/style.css")){
			    	messages.add("そのページは、総務人事部以外アクセスできません。");
					session.setAttribute("errorMessages", messages);
			    	((HttpServletResponse)response).sendRedirect("./");
			    	return;
		    	}
		    }
        }
        if(((HttpServletRequest)request).getServletPath().equals("/management")){
		    if(user.getPositionId() != 1){
		    	if(!((HttpServletRequest)request).getServletPath().equals("/css/style.css")){
			    	messages.add("そのページは、総務人事部以外アクセスできません");
					session.setAttribute("errorMessages", messages);
			    	((HttpServletResponse)response).sendRedirect("./");
			    	return;
		    	}
		    }
        }
        if(((HttpServletRequest)request).getServletPath().equals("/signup")){
		    if(user.getPositionId() != 1){
		    	if(!((HttpServletRequest)request).getServletPath().equals("/css/style.css")){
			    	messages.add("そのページは、総務人事部以外アクセスできません");
					session.setAttribute("errorMessages", messages);
			    	((HttpServletResponse)response).sendRedirect("./");
			    	return;
		    	}
		    }
        }


        chain.doFilter(request, response); // サーブレットを実行
    }

    @Override
    public void init(FilterConfig config) {

    }
    @Override
    public void destroy() {
    }

}
